# typs.py

import queue
import asyncio


class Mail:
 
    def __init__(self, owner, target, text, audio):
        self.owner = owner
        self.target = target
        self.text = text
        self.audio = audio


class MailBox:

    def __init__(self, name):

        self.name = name # just for logging purposes.
        self.inbox = asyncio.Queue()
        self.outbox = queue.Queue()


class Intent:

    def __init__(self, name, params: dict):
        self.name = name
        self.params = params