# server.py

import os
import json
import uuid
import asyncio
import websockets

import typs
import session

HOST = os.getenv('HOST')
PORT = os.getenv('PORT')

# Makes instance of a Messenger session for every client that connects.
async def launch_session(crimata_id, connection):

    # Create the session.
    s = session.Session(connection, crimata_id)

    # Run core loops.
    await asyncio.gather(

        s.do_agent_intents(),

        s.deliver_mail()

    )

    print("Ended session.")

async def main(connection, path):
    crimata_id = await connection.recv()

    # Start session with token.
    await launch_session(crimata_id, connection) #! Not stopping on disconnect

# Run run for every new connection.
async def lift():
    print(f"Listening for connections on {HOST}:{PORT}")
    await websockets.serve(main, HOST, PORT)