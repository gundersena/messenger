# messenge.py

import asyncio

import mailroom


# Independent loop that updates mailboxes.
async def messenger():    

    while True:

        # Yield if no mailboxes.
        if mailroom.que.empty():
            await asyncio.sleep(1)
            continue 

        # Grab a mailbox.
        mailbox = mailroom.que.get()

        # Handle every message in outbox.
        while not mailbox.outbox.empty():
            message = mailbox.outbox.get()

            # Assert mailbox for target.
            if message.target not in mailroom.subs:
                mailroom.create_mailbox(message.target)

            # Get mailbox of target and put mail there.
            target_mailbox = mailroom.get_mailbox(message.target)
            print(f"{mailbox.name} -> {target_mailbox.name}: {message.text}")
            await target_mailbox.inbox.put(message)

        # Put the mailbox back
        mailroom.que.put(mailbox)
        await asyncio.sleep(0.1)

async def run():
    await messenger()