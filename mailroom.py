# mailroom.py

import queue
import asyncio

import typs

# Subcribers (Crimata IDs)
subs = []

# Mailroom (all mailboxes)
que = queue.Queue()
ref = {}

# Mailroom functions
# ------------------

# Create mailbox and add to mailroom.
def create_mailbox(crimata_id):
	mailbox = typs.MailBox(name=crimata_id)
	que.put(mailbox)
	ref.update({crimata_id: mailbox})
	subs.append(crimata_id)

# Get mailbox by Crimata ID.
def get_mailbox(crimata_id):
	if not crimata_id in subs:
		create_mailbox(crimata_id)
	mailbox = ref.get(crimata_id)
	return mailbox

# Inbox -> receiving mail.
async def get_mail(crimata_id):
	mailbox = get_mailbox(crimata_id)
	mail = await mailbox.inbox.get()
	return mail

# Outbox -> sending mail. 
def put_mail(crimata_id, mail: typs.Mail):
	mailbox = get_mailbox(crimata_id)
	mailbox.outbox.put(mail)
	