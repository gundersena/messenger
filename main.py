# server.py

import typs
import queue
import asyncio
import websockets

import server
import message


# Run the ws server and the messenger concurrently.
async def main():
    print(f"Crimata Messenger")
    await asyncio.gather(server.lift(), message.run())

if __name__ == '__main__':
    asyncio.run(main())
