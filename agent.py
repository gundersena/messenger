import json
import asyncio
import websockets

import typs


class Agent:
    """Session interface for Crimata Agent

    Receive core functionalities for communicating with Crimata 
    Agent in an OOP way.

    """
    def __init__(self, connection):
        self.service = None
        self.client_connection = connection

    async def recv(self):
        package = await self.client_connection.recv()
        jpackage = self.__decode(package)
        return jpackage

    async def send(self, jpackage):
        package = self.__encode(jpackage)
        await self.client_connection.send(package)

    def __encode(self, jpackage):
        package = json.dumps(jpackage)
        return package

    def __decode(self, package):
        jpackage = json.loads(package)
        return jpackage




