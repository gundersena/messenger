# mail.py

import typs
import mailroom


class Mail:
	"""Session interface for mailroom.

	Two main IO methods. Will translate intents to mail and vice versa.
	Should be a parent class of Session.
	"""
	def __init__(self, crimata_id):
		self.crimata_id = crimata_id

	# Receive intent.
	async def mailbox_recv(self):
		mail = await mailroom.get_mail(self.crimata_id)
		intent = self.__decode(mail)
		return intent

	# Send intent.
	async def mailbox_send(self, jpackage):
		mail = self.__encode(jpackage)
		print(f"Putting message into outbox {self.crimata_id}")
		mailroom.put_mail(self.crimata_id, mail)
		await self.send(1) # agent confirmation.

	def __encode(self, jpackage) -> typs.Mail:
		j = jpackage
		owner = self.crimata_id
		target = j.get("target")
		text = j.get("text")
		audio = j.get("audio")
		mail = typs.Mail(owner, target, text, audio)
		return mail

	def __decode(self, mail: typs.Mail):
		jpackage = {
			"owner": mail.owner,
			"target": self.crimata_id,
			"text": mail.text, #dev only
			"audio": mail.audio
		}
		return jpackage